function plusMinus(arr) {
	// body...
	// Variabel untuk menampung mana yang +, -, 0

	let positive = 0;
	let negative = 0;
	let netral = 0;

	// Perulangan untuk mengambil data perIndex
	for (var i = 0; i < arr.length; i++) {
		
		// Kondisi jika nilainya positive
		if (arr[i] > 0) {
			positive += 1;
		}

		// Kondisi jika nilainya negative
		else if (arr[i] < 0) {
			negative += 1;
		}

		// Kondisi jika nilainya 0
		else {
			arr[i] == 0;
			netral += 1;
		}
	}

	console.log((positive/arr.length).toFixed(5));
	console.log((negative/arr.length).toFixed(5));
	console.log((netral/arr.length).toFixed(5));
}

console.log(plusMinus([-2,-1,0,1,2]));